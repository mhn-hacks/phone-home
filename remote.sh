#!/bin/bash

if [ $# -lt 4 ]
then
  >&2 echo "Usage: $(basename $0) remote_socket_file index_file host_name user [additional_remote_socket_file...]"
  exit 1
fi

home=$(pwd)

REMSOCK="$home/$1"
INDEX="$home/$2"
HOST="$3"
USER="$4"
shift
shift
shift
shift

if [ ! -e "$REMSOCK" ] || rm "$REMSOCK"
then
  >&2 echo "Removed remote socket '$REMSOCK' (or it did not exist)"

  for sock in "$@"
  do
    [ -e "$sock" ] && rm "$sock" && >&2 echo "Removed additional socket file $sock"
  done

  touch "$INDEX"
  grep -q "^$HOST " "$INDEX" || echo "$HOST user" >> "$INDEX"
  sed -i "/^$HOST /c\\$HOST $USER" "$INDEX"
  echo "$home"
  cat "$INDEX"
else
  >&2 echo "Could not remove remote socket"
  exit 1
fi

exit 0
