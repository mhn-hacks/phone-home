#!/bin/bash

cd "${BASH_SOURCE%/*}" || exit

if [ $# -lt 1 ]
then
  echo "Usage: $(basename $0) user@remotehost [additional_port [additional_port]]"
  exit 0
fi

remotehost="$1"

shift

key="$HOME/.ssh/tunnel_id"

[ -e "$key" ] || ssh-keygen -N "" -f "$key"

ssh-copy-id -i "$key" $remotehost || exit 1

encoded=$(systemd-escape --template=phone-home@.service "$remotehost")

srvdir="$HOME/.config/systemd/user"
srvtarget="$srvdir/$encoded"

mkdir -p "$srvdir"

cat > "$srvtarget" <<EOF
[Unit]
Description=Phone home SSH service for host $remotehost
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=$(pwd)/phone-home.sh "$remotehost" $@

RestartSec=120
Restart=always

[Install]
WantedBy=default.target
EOF

systemctl --user daemon-reload
systemctl --user enable "$encoded"
systemctl --user start "$encoded"

user=$(whoami)

echo "To enable the service to start before the user logs in, enter root password"
echo "(loginctl enable-linger $user)."

su -c "loginctl enable-linger $user"
