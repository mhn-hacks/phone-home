#!/bin/bash

cd "${BASH_SOURCE%/*}" || exit

if [ $# -lt 1 ]
then
  echo "Usage: $(basename $0) user@remotehost"
  exit 0
fi

remotehost="$1"

encoded=$(systemd-escape --template=phone-home@.service "$remotehost")

srvdir="$HOME/.config/systemd/user"
srvtarget="$srvdir/$encoded"

systemctl --user stop "$encoded"
systemctl --user disable "$encoded"

rm $srvtarget

systemctl --user daemon-reload
