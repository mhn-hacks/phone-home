Phone Home SSH Service
======================

A systemd user service for creating connections back from a shared remote host, and to other connected clients.

## Dependencies:

### Client
* openssh 6.7 or greater (needed for socket forwarding)
* netcat-openbsd (needs -U parameter).

### Server
* openssh 6.7 or greater (needed for socket forwarding)

## Installation
Run `./install.sh user@remote_host`. It will set up a passwordless ssh id key used to authenticate with the remote host. It also sets up the systemd service.

## Uninstallation
Run `./uninstall.sh user@remote_host`.

